package persistence;

import java.io.*;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/***
* Esta clase implementa un singletón cuya característica es que sólo es accesible mediantes sus métodos para escribir en un fichero
*/

public class SingletonStaticPI{

    private  File txtFile;
    private  PrintWriter txtPrintWriter;
    private  boolean isInitialized = false;

    private static SingletonStaticPI singleInstance=null;
        
    private SingletonStaticPI()throws FileNotFoundException {
		open();
    }
		
    //s�lo lo vamnos a llamar desde los m�todos writeX, por lo que desde fuera, los �nicos m�todos putlicos ser�n los writeX. As�, tenemos un singlet�n que gestionamos desde dentro de la clase TraceWritter...a ver si no peta.
    private static SingletonStaticPI getInstance() throws FileNotFoundException {
    
        singleInstance = new SingletonStaticPI();
	
        return singleInstance;
    }
  
    
    //No tengo muy claro c�mo funciona el "isInitialized"...supongo que es para ver si existe el fichero verdad?
    private void open()throws FileNotFoundException{
    	
    	if(!isInitialized) {
    		try {
    			//new File(dirFilename).mkdirs();
    			
			txtFile = new File("./SingletonStaticPI_"+System.currentTimeMillis()+".txt");
			//txtFilename = txtFile.getAbsolutePath();
    			isInitialized = true;
    		}
    		catch(Exception e) {
    			System.out.println("There is a problem while creating Log File " + e.toString() );
    		}
    	}
    	
    		/*
		try {
			if(!txtFile.createNewFile()) {
				//txtFile.delete();
				//txtFile.createNewFile();
			}else {
				//txtFile = new File(txtFilename);
				//txtFilename = txtFile.getAbsolutePath();
			}			
		}catch(Exception e) {
			System.out.println("Unable to create Log File " + e.toString() );
		}
		*/
		
			txtPrintWriter = new PrintWriter(txtFile);
    }
    
    public boolean isValidPath(String path) {
        try {
            Paths.get(path);
        } catch (InvalidPathException | NullPointerException ex) {
            return false;
        }
        return true;
    }


//Nuevas versiones de los m�todos write. Esto se sigue invicando desde fuera como TraceWritter.writeTxt("xsssssssss");
    public static void writeTxt(String text) throws Exception{
		
			singleInstance = getInstance();
			//AL CREAR LA NUEVA INSTANCIA, SE ABRE EL FICHERO, AS� NO HAY QUE ABRIR/CERRAR
			singleInstance.writeTxtInstance(text);
	    		    	
    }
    
    
    private void writeTxtInstance(String text) throws Exception{
    		txtPrintWriter.append(text + "\r\n");
    		txtPrintWriter.flush();
    }
    
    public static void closeExecution() throws Exception{
		    singleInstance = getInstance();
    		singleInstance.closeFile();
    		singleInstance = null;
    }
    
    public void closeFile() throws Exception{
    		this.txtPrintWriter.close();
    }
}