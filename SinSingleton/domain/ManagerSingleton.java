package domain;

import java.io.FileNotFoundException;

import persistence.SingletonStaticPI;

public class ManagerSingleton {

	public static boolean throwClassicSingletonTest(String path, String recordText, int numRecord) {
		boolean sucess = false;
		
		return sucess;
	}
	
	public static void throwStaticAPISingletonTest(String path, String recordText, int numRecord) {
		boolean sucess = false;
		
		try {
			StatusObserver.notifyMsg("===============\n\tStarting writting "+numRecord+" records - "+System.currentTimeMillis());
			for(int i=0; i<numRecord; i++)
				SingletonStaticPI.writeTxt(recordText+"_"+i);
			SingletonStaticPI.closeExecution();
			StatusObserver.notifyMsg("\n\tFinishing writting "+numRecord+" records - "+System.currentTimeMillis()+"\n===============\n");
		}catch(FileNotFoundException fnfe) {
			StatusObserver.notifyMsg("Error en fichero:\n"+fnfe.toString());
		}catch(Exception e) {
			StatusObserver.notifyMsg("Error:\n"+e.toString());
		}
	}

	public static boolean throwFullStaticSingletonTest(String path, String recordText, int numRecord) {
		boolean sucess = false;
		
		return sucess;
	}

	
}
