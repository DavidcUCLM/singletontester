package domain;

import javax.swing.JFrame;
import presentation.JDesignPatternTester;

public class StatusObserver {
	private static JDesignPatternTester jdp;
	
	public static void setJFrameObserved(JFrame frame) {
		jdp = (JDesignPatternTester)frame;
	}
	
	public static void notifyMsg(String msg) {
		jdp.notifyStatus(msg);
	}
}
