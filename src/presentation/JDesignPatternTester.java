package presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import domain.ManagerSingleton;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;

public class JDesignPatternTester extends JFrame {

	private JPanel contentPane;
	private final JButton btnLaunchTest = new JButton("Launch test");
	private JTextField textFieldRecord;
	private JTextField textFieldPath;
	private JTextPane textPaneStatus;
	private JCheckBox chckbxClassicSingleton;
	private JCheckBox chckbxStaticApiSingleton;
	private JCheckBox chckbxFullStaticSingleton;
	private JSpinner spinnerNumberRecords;
	private JScrollPane scrollPaneStatus;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JDesignPatternTester frame = new JDesignPatternTester();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JDesignPatternTester() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 572, 542);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
			
		chckbxClassicSingleton = new JCheckBox("Classic Singleton");
		chckbxClassicSingleton.setBounds(6, 72, 175, 23);
		contentPane.add(chckbxClassicSingleton);
		
		chckbxStaticApiSingleton = new JCheckBox("Static API Singleton");
		chckbxStaticApiSingleton.setBounds(6, 95, 163, 23);
		contentPane.add(chckbxStaticApiSingleton);
		
		chckbxFullStaticSingleton = new JCheckBox("Full Static Singleton");
		chckbxFullStaticSingleton.setBounds(6, 118, 176, 23);
		contentPane.add(chckbxFullStaticSingleton);
		
		spinnerNumberRecords = new JSpinner();
		spinnerNumberRecords.setBounds(226, 94, 108, 26);
		contentPane.add(spinnerNumberRecords);
		
		btnLaunchTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String path = JDesignPatternTester.this.textFieldPath.getText();
				String recordText = JDesignPatternTester.this.textFieldRecord.getText();
				int numRecords = Integer.parseInt(JDesignPatternTester.this.spinnerNumberRecords.getValue().toString());
				
				if(JDesignPatternTester.this.chckbxFullStaticSingleton.isSelected())
					ManagerSingleton.throwFullStaticSingletonTest(path, recordText, numRecords);
				if(JDesignPatternTester.this.chckbxClassicSingleton.isSelected()) 
					ManagerSingleton.throwClassicSingletonTest(path, recordText, numRecords);		
				if(JDesignPatternTester.this.chckbxStaticApiSingleton.isSelected())
					ManagerSingleton.throwStaticAPISingletonTest(path, recordText, numRecords);		
				
			}
		});
		btnLaunchTest.setBounds(6, 178, 117, 29);
		contentPane.add(btnLaunchTest);
		

		
		
		
		JButton btnNewButton = new JButton("All");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chckbxClassicSingleton.setSelected(true);
				chckbxStaticApiSingleton.setSelected(true);
				chckbxFullStaticSingleton.setSelected(true);
			}
		});
		btnNewButton.setBounds(6, 142, 64, 29);
		contentPane.add(btnNewButton);
		
		JButton btnNone = new JButton("None");
		btnNone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chckbxClassicSingleton.setSelected(false);
				chckbxStaticApiSingleton.setSelected(false);
				chckbxFullStaticSingleton.setSelected(false);
			}
		});
		btnNone.setBounds(70, 142, 64, 29);
		contentPane.add(btnNone);
		
		textFieldRecord = new JTextField();
		textFieldRecord.setText("This is the record that will be written");
		textFieldRecord.setBounds(6, 34, 424, 26);
		contentPane.add(textFieldRecord);
		textFieldRecord.setColumns(10);
		
		JLabel lblInsertBelowThe = new JLabel("Insert below the text to be saved in the target files by the Singleton Writters");
		lblInsertBelowThe.setLabelFor(textFieldRecord);
		lblInsertBelowThe.setBounds(6, 17, 522, 16);
		contentPane.add(lblInsertBelowThe);
		

		
		JLabel lblNumberOfRecords = new JLabel("Number of records to be written");
		lblNumberOfRecords.setBounds(229, 76, 217, 16);
		contentPane.add(lblNumberOfRecords);
		
		JLabel lblLogFilesPath = new JLabel("Log Files Path (Local to project by default)");
		lblLogFilesPath.setBounds(226, 122, 271, 16);
		contentPane.add(lblLogFilesPath);
		
		textFieldPath = new JTextField();
		textFieldPath.setText("./");
		textFieldPath.setBounds(226, 142, 130, 26);
		contentPane.add(textFieldPath);
		textFieldPath.setColumns(10);
		
		JLabel lblStatus = new JLabel("Status");
		lblStatus.setBounds(16, 208, 61, 16);
		contentPane.add(lblStatus);
		
		textPaneStatus = new JTextPane();
		textPaneStatus.setEditable(false);
		textPaneStatus.setBounds(6, 229, 559, 118);
		//contentPane.add(textPaneStatus);
		
		scrollPaneStatus = new JScrollPane();
		scrollPaneStatus.setBounds(6, 366, 559, 135);
		contentPane.add(scrollPaneStatus);
		domain.StatusObserver.setJFrameObserved(this);
	}

	public void notifyStatus(String msg) {
		this.scrollPaneStatus.setViewportView(this.textPaneStatus);
		this.textPaneStatus.setText(this.textPaneStatus.getText()+msg);
	}
}
